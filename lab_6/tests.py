from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab6_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, index)

